import sys, os

from config import *

if __name__ == "__main__":
    print("miner settings!\n")

    for a in config_list:
        mod_config(a[0], a[1], a[2], a[3])

    try:
        f = open(config_path + 'miner', 'r')
        miner = f.read()
        f.close()

    except FileNotFoundError:
        pass

    else:
        for a in miner_config_dict[miner]:
            mod_config(a[0], a[1], a[2], a[3])
