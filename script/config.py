import os, sys

base_path = os.path.abspath(os.path.dirname(sys.argv[0]))
config_path = base_path + '/../config/'
log_path = base_path + '/../log/'

config_list = [
        ['miner name', None, None, 'miner_name'],
        ['miner account', None, 'emato', 'miner_account'],
        ['miner', ['ethereum', 'zcash', 'monero'], 'ethereum', 'miner'],
        ['power limit percent (nvidia only)', 'int', 75, 'nvidia_powerlimit'],
        ['core clock to increase (nvidia only)', 'int', 1, 'nvidia_coreclock'],
        ['memory clock to increase (nvidia only)', 'int', 1, 'nvidia_memoryclock'],
]

ethereum_config_list = [
        ['miner port', ['20535', '20555', '20565', '20585'], '20535', 'miner_port'],
        ['claymore pascal dual mining (true -> 1, false -> 0)', 'int', '0', 'claymore_dual'],
        ['claymore dcri', 'int', 13, 'claymore_dcri'],
]

zcash_config_list = [
        ['miner port', ['20570', '20595', '20575', '20594'], '20535', 'miner_port'],
]

monero_config_list = [
        ['miner port', ['20570', '20595', '20575', '20594'], '20535', 'miner_port'],
        ['monero7 algorithm', 'int', 1, 'miner_monero_pow7'],
]

miner_config_dict = {
        'ethereum': ethereum_config_list,
        'zcash': zcash_config_list,
        'monero': monero_config_list,
}

def get_config(config_name):
    try:
        f = open(config_path + config_name, 'r')
        buf = f.read()
        f.close()

    except FileNotFoundError:
        return None

    return buf.strip()

def set_config(config_name, data):
    f = open(config_path + config_name, 'w')
    f.write(str(data))
    f.close()

def mod_config(name, typelimit, default, filename):
    current = get_config(filename)

    new = input_prompt(name, typelimit, default, current)

    set_config(filename, new)

def input_prompt(name, typelimit, default = None, current = None):
    #typelimit None, "int"

    while True:
        print("input value %s " % name, end='')
        if typelimit == "int": print("(Number only) ", end='')
        if isinstance(typelimit, list): print('(%s) ' % ', '.join(typelimit), end='')
        if default: print('Default %s ' % default, end='')
        if current: print('Current %s ' % current, end='')

        buf = input(" : ")

        if len(buf) == 0:
            if current:
                print('keep current')
                ret = current
                break

            elif default:
                print('set default')
                ret = default
                break

            else:
                print('woring input re-try')
                continue

        elif typelimit == "int":
            try:
                ret = int(buf)

            except:
                print("wrong input re-try")
                continue

            break

        elif isinstance(typelimit, list):
            if buf in typelimit:
                ret = buf
                break

            else:
                print("wrong input re-try")
                continue

        else:
            ret = buf.strip()
            break

    return ret

