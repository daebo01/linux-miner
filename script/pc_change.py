#/sys/class/net/eth0/address
# config -> macaddress
# != pc name , chrome settings delete
import sys, os, subprocess

config_path = os.path.abspath(os.path.dirname(sys.argv[0])) + '/../config/'

def save_dhclient_conf():
    try:
        f = open(config_path + 'miner_name', 'r')
        hostname = f.read()
        f.close()

    except FileNotFoundError:
        hostname = 'default'

    try:
        f = open(config_path + 'dhclient.conf-default', 'r')
        conf = f.read()
        f.close()

    except FileNotFoundError:
        print('not find default dhclient.conf')
        return

    
    try:
        f = open(config_path + 'dhclient.conf', 'w')
        f.write(conf.replace('{HOSTNAME}', hostname))
        f.close()

    except FileNotFoundError:
        print('save fail dhclient.conf')
        return

def check_macaddress():
    try:
        f = open('/sys/class/net/eth0/address', 'r')
        current = f.read()
        f.close()

    except FileNotFoundError:
        print('? eth0 not fount')
        current = 'current'

    try:
        f = open(config_path + 'macaddress', 'r')
        conf = f.read()
        f.close()

    except FileNotFoundError:
        conf = 'conf'

    if current == conf:
        return

    try:
        print('macaddress diff')
        subprocess.check_output('rm -rf /var/lib/dhcp/dhclient.eth0.leases; exit 0', stderr=subprocess.STDOUT, shell=True)
        subprocess.check_output('rm -rf /home/ematoyo/.config; exit 0', stderr=subprocess.STDOUT, shell=True)

    except FileNotFoundError:
        1+1

    f = open(config_path + 'macaddress', 'w')
    f.write(current)
    f.close()


if __name__ == '__main__':
    save_dhclient_conf()
    check_macaddress()
