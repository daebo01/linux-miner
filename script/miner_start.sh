#!/bin/bash
screen -q -X -S mining1 quit

miner=`cat ~/miner/config/miner`

if [ "$miner" == "ethereum" ]
then
	miner_script=~/miner/claymore/start.sh
elif [ "$miner" == "zcash" ]
then
	miner_script=~/miner/ewbf/start.sh
elif [ "$miner" == "monero" ]
then
	miner_script=~/miner/monero/start.sh
else
	miner_script="while true; do echo not found miner! $miner please re setting and reboot; sleep 1; done"
fi

screen -amdS mining1 sh -c "$miner_script"

~/miner/script/emms_start.sh
