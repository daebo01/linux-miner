import sys, os, subprocess, time

config_path = os.path.abspath(os.path.dirname(sys.argv[0])) + '/../config/'

def get_config(config_name):
    try:
        f = open(config_path + config_name, 'r')
        buf = f.read()
        f.close()

    except FileNotFoundError:
        buf = 'notfound'

    return buf.strip()


if __name__ == "__main__":
    minername = get_config('miner_name')
    miner = get_config('miner')
    mineraccount = get_config('miner_account')
    minerport = get_config('miner_port')
    last_update = subprocess.check_output('cd ~/miner;git log -1 --format=%cd --date=short;exit 0;', stderr=subprocess.STDOUT, shell=True).decode('utf-8').strip()
    while True:
        ip = subprocess.check_output('/sbin/ifconfig eth0 | grep "inet" | cut -d: -f2 | awk \'{ print $2 }\'', stderr=subprocess.STDOUT, shell=True).decode('utf-8').strip()
        print('linux miner! %s, %s, %s, %s, %s, %s' % (minername, mineraccount, miner, minerport, ip, last_update))
        time.sleep(1)
