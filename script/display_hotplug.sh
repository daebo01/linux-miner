#!/bin/bash

export DISPLAY=:0
export XAUTHORITY=/home/ematoyo/.Xauthority

if [[ $(cat /sys/class/drm/card0-HDMI-A-1/status | grep -Ec "^connected") -eq 1 ]]; then
	#~/miner/script/xserver_restart.sh
	/bin/sleep 2s;
	/usr/bin/xrandr --verbose --output HDMI1 --auto 2>&1 >> /var/log/hotplug.log;
fi

if [[ $(cat /sys/class/drm/card0-HDMI-A-2/status | grep -Ec "^connected") -eq 1 ]]; then
	#~/miner/script/xserver_restart.sh
	/bin/sleep 2s;
	/usr/bin/xrandr --verbose --output HDMI2 --auto 2>&1 >> /var/log/hotplug.log;
fi

#if [[ $(cat /sys/class/drm/card0-DP-1/status | grep -Ec "^connected") -eq 1 ]]; then
#        /bin/sleep 2s;
#        /usr/bin/xrandr --verbose --output DP1 --auto 2>&1 >> /var/log/hotplug.log;
#fi

if [[ $(cat /sys/class/drm/card0-VGA-1/status | grep -Ec "^connected") -eq 1 ]]; then
	#~/miner/script/xserver_restart.sh
	/bin/sleep 2s;
	/usr/bin/xrandr --verbose --output VGA1 --auto 2>&1 >> /var/log/hotplug.log;
fi
