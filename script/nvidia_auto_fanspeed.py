import subprocess

gpu_temp_fanspeed = [
        [80, 100],
        [75, 90],
        [70, 80],
        [60, 70],
        [50, 60],
        [1, 10],
]

def set_fanspeed(gpuid, speed):
    buf = subprocess.check_output('DISPLAY=:0 XAUTHORITY=/home/ematoyo/.Xauthority nvidia-settings -a "[gpu:%s]/GPUFanControlState=1" -a "[fan:%s]/GPUTargetFanSpeed=%s"; exit 0' % (gpuid, gpuid, speed), stderr=subprocess.STDOUT, shell=True)
    print(buf)

if __name__ == "__main__":
    buf = subprocess.check_output('/usr/bin/nvidia-smi --format=csv --query-gpu=temperature.gpu', shell=True)
    temp = buf.decode('utf-8').strip().split('\n')[1:]
    for a in range(0, len(temp)):
        loop = True
        for b in gpu_temp_fanspeed:
            if loop == True and int(temp[a]) > b[0]:
                print('gpu %s temp %s speedset %s' % (a, temp[a], b[1]))
                set_fanspeed(a, b[1])
                loop = False

