import subprocess, os, sys, datetime
from config import *

if __name__ == "__main__":
    buf = subprocess.check_output('sudo dmesg | grep Xid;exit 0', stderr=subprocess.STDOUT, shell=True)
    uptime = int(float(open('/proc/uptime', 'r').read().split(' ')[0]))
    loadavg = int(float(open('/proc/loadavg', 'r').read().split(' ')[0]))
    if buf or loadavg > 10:
        buf = buf.decode('utf-8')

        f = open(log_path + 'gpuerror.log', 'a')
        f.write("[%s %s] gpu error detect\n" % (str(datetime.datetime.now().date()), str(datetime.datetime.now().time())))
        f.write(buf)
        f.close()

        print("gpu error detect")

        if uptime < 86400:
            coreclock = int(get_config("nvidia_coreclock"))
            memoryclock = int(get_config("nvidia_memoryclock"))

            if coreclock > 2:
                set_config("nvidia_coreclock", coreclock - 1)

            if memoryclock > 2:
                set_config("nvidia_memoryclock", memoryclock - 1)

        subprocess.check_output('~/miner/script/forcereboot.sh', stderr=subprocess.STDOUT, shell=True)
