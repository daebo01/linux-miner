#!/bin/bash

XPID=$(ps aux | grep [x]serverrc | awk '{ print $2}')
UID=$(id -u)

kill $XPID

sleep 3

if [ $UID -eq 0 ]
then
	su ematoyo -c startx -- :0 &
else
	startx -- :0 &
fi

