import subprocess,sys,os

config_path = os.path.abspath(os.path.dirname(sys.argv[0])) + '/../config/'

def set_fanspeed(gpuid, speed):
    buf = subprocess.check_output('DISPLAY=:0 XAUTHORITY=/home/ematoyo/.Xauthority nvidia-settings -a "[gpu:%s]/GPUFanControlState=1" -a "[fan:%s]/GPUTargetFanSpeed=%s"; exit 0' % (gpuid, gpuid, speed), stderr=subprocess.STDOUT, shell=True)
    print(buf)

def set_powerlimit(name, powerwatt, powermin, powermax):
    try:
        f = open(config_path + 'nvidia_powerlimit', 'r')
        per = f.read()
        f.close()

        watt = int(float(powerwatt) * float(per) / 100)

        if watt > int(powermax): watt = powermax
        if watt < int(powermin): watt = powermin

        buf = subprocess.check_output('sudo /usr/bin/nvidia-smi -pm 1;exit 0', stderr=subprocess.STDOUT, shell=True)
        print(buf)
        buf = subprocess.check_output('sudo /usr/bin/nvidia-smi -pl %s;exit 0' % watt, stderr=subprocess.STDOUT, shell=True)
        print(buf)

    except FileNotFoundError:
        print('powerlimit not set')
        return

def set_clock(gpuid, gpuname):
    targetnum = 3
    if gpuname == "P106-090":
        targetnum = 1

    try:
        f = open(config_path + 'nvidia_coreclock', 'r')
        coreclock = f.read()
        f.close()

        f = open(config_path + 'nvidia_memoryclock', 'r')
        memoryclock = f.read()
        f.close()

    except FileNotFoundError:
        print('clock not set')
        return

    buf = subprocess.check_output('DISPLAY=:0 XAUTHORITY=/home/ematoyo/.Xauthority nvidia-settings -a "[gpu:%s]/GPUGraphicsClockOffset[%s]=%s"; exit 0' % (gpuid, targetnum, coreclock), stderr=subprocess.STDOUT, shell=True)
    print(buf)
    buf = subprocess.check_output('DISPLAY=:0 XAUTHORITY=/home/ematoyo/.Xauthority nvidia-settings -a "[gpu:%s]/GPUMemoryTransferRateOffset[%s]=%s"; exit 0' % (gpuid, targetnum, int(memoryclock) * 2), stderr=subprocess.STDOUT, shell=True)
    print(buf)
    buf = subprocess.check_output('DISPLAY=:0 XAUTHORITY=/home/ematoyo/.Xauthority nvidia-settings -a "[gpu:%s]/GpuPowerMizerMode=1"; exit 0' % (gpuid), stderr=subprocess.STDOUT, shell=True)
    print(buf)


if __name__ == "__main__":
    buf = subprocess.check_output('/usr/bin/nvidia-smi --format=csv --query-gpu=name', shell=True)
    name = buf.decode('utf-8').strip().split('\n')[1:]
    buf = subprocess.check_output('/usr/bin/nvidia-smi --format=csv --query-gpu=power.default_limit,power.min_limit,power.max_limit', shell=True)
    power = buf.decode('utf-8').strip().split('\n')[1:]
    power2 = power[0].split(',')

    set_powerlimit(name[0], power2[0].split('.')[0], power2[1].split('.')[0], power2[2].split('.')[0])

    for a in range(0, len(name)):
        set_clock(a, name[a])

