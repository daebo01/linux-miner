#!/bin/bash

#sed 's/sudo killall ethdcrminer64/screen -S -X mining1 quit;sleep 2/' /etc/bash.bashrc
if [ $(cat /etc/bash.bashrc | grep -c "command_alias") -gt 1 ]
then
	cat /etc/bash.bashrc | grep -v command_alias > /tmp/tmptmp
	sudo sh -c "cat /tmp/tmptmp > /etc/bash.bashrc"
fi

#if [ $(cat /etc/bash.bashrc | grep -c "command_alias.sh") -eq 0 ]
#then
	sudo sh -c "echo source `readlink -f ~/miner/script/command_alias.sh` >> /etc/bash.bashrc"
#fi

# needs_root_rights=yes
if [ $(cat /etc/X11/Xwrapper.config | grep -c "needs_root_rights") -eq 0 ]
then
	sudo sh -c "echo needs_root_rights=yes >> /etc/X11/Xwrapper.config"
fi

if [[ -n $(diff -q /home/ematoyo/miner/config/system/grub /etc/default/grub) ]]
then
	sudo cp `readlink -f ~/miner/config/system/grub` /etc/default/grub
	sudo update-grub
fi

cp ~/miner/config/system/chrome_remote ~/.chrome-remote-desktop-session
sudo cp `readlink -f ~/miner/config/system/crontabs_root` /var/spool/cron/crontabs/root
sudo cp `readlink -f ~/miner/config/system/xorg.conf` /etc/X11/xorg.conf
cp ~/miner/config/system/git_config ~/miner/.git/config

mkdir -p ~/.ssh/
cp ~/miner/util/git_key/linux-miner-git ~/.ssh/id_rsa
chmod 600 ~/.ssh/id_rsa
