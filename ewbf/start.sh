#!/bin/sh
cd ~/miner/ewbf
ln -s ../reboot.sh ewbf/reboot.sh
python3 ~/miner/script/nvidia_overclock.py
sleep 3
cp noappend.log prev.log -f
if [ -e ~/miner/config/miner_port ] && [ -e ~/miner/config/miner_account ] && [ -e ~/miner/config/miner_name ]
then
	ewbf/miner --server asia.equihash-hub.miningpoolhub.com --user `cat ~/miner/config/miner_account`.`cat ~/miner/config/miner_name` --pass x --port `cat ~/miner/config/miner_port` --eexit 1 --fee 0 --api 0.0.0.0:3334 --intensity 64 --solver 0 --log 2 --logfile noappend.log
	sleep 1
	./reboot.sh
	bash
else
	echo "init-settings.py first run!"
fi
